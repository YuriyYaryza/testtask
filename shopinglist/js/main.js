function clkAdd(){
    var nameprod=document.getElementById('nameprod').value;
    var priceprod=document.getElementById('priceprod').value;
    $.ajax({
        method: "POST",
        url: "action/insert.php",
        data: {name : nameprod, price : priceprod},
        dataType: "html"
    }).done(function(response) {
        $('#table_wrapper').html( response );
    });
}

function clickDelete(product_id){
    $.ajax({
        method: "POST",
        url: "action/delete.php",
        data: {product_id : product_id},
        dataType: "html"
    }).done(function(response) {
        $('#table_wrapper').html( response );
    });
}